// craco.config.js
const path = require(`path`);
module.exports = {
  style: {
    postcss: {
      plugins: [require("tailwindcss"), require("autoprefixer")],
    },
  },

  webpack: {
    alias: {
      "@": path.resolve(__dirname, "src/"),
      "@components": path.resolve(__dirname, "src/components"),
      "@language": path.resolve(__dirname, "src/components/languages/index.js"),
      "@assets": path.resolve(__dirname, "src/assets"),
      "@noImg": path.resolve(__dirname, "src/assets/images/noimg.png"),
      "@UsdIocn": path.resolve(__dirname, "src/assets/images/usd.png"),
      "@QarIocn": path.resolve(__dirname, "src/assets/images/qar.png"),
      "@noProfileImg": path.resolve(
        __dirname,
        "src/assets/images/noprofile.png"
      ),
      "@constant": path.resolve(__dirname, "src/common/constants"),
      "@baseComponent": path.resolve(__dirname, "src/common/base-component"),
      "@commonFunction": path.resolve(__dirname, "src/common/common-functions"),
      "@countryList": path.resolve(__dirname, "src/common/country-list"),
      // "@noContent": path.resolve(__dirname, "src/components/Common/no-content"),
      "@divider": path.resolve(__dirname, "src/common/divider"),
      "@noDataFound": path.resolve(
        __dirname,
        "src/common/no-data-found-basecomponet.js"
      ),
      "@noContentSlideOver": path.resolve(
        __dirname,
        "src/common/no-content-slideover.js"
      ),
      "@networkCall": path.resolve(__dirname, "src/common/network-call"),
      "@baseLoader": path.resolve(__dirname, "src/common/base-loader"),
      "@toasrFeatureunavailable": path.resolve(
        __dirname,
        "src/common/feature-under-development-toast"
      ),
      "@btnLoaderSvg": path.resolve(__dirname, "src/common/btn-loader-svg"),
      "@commonFolder": path.resolve(__dirname, "src/common"),
      "@reportAbuse": path.resolve(
        __dirname,
        "src/components/dashboard/profilesettings/components/myfollowers/report-abuse"
      ),
      "@reportAbuseQa": path.resolve(
        __dirname,
        "src/components/dashboard/profilesettings/components/myfollowers/report-abuse-qa"
      ),

      /* MAIN [Global/First] */
      "@mainActions": path.resolve(__dirname, "src/actions/index"),
      "@mainReducers": path.resolve(__dirname, "src/reducers/index"),

      /* STARTER */
      "@starterCss": path.resolve(
        __dirname,
        "src/components/starter/starter.css"
      ),
      /* STARTER */
      "@starter": path.resolve(__dirname, "src/components/starter"),
      /* SALES MANAGEMNT */
      "@salesmanagement": path.resolve(
        __dirname,
        "src/components/dashboard/salesmanagement/components/index"
      ),
      "@salesmanagementSalesTab": path.resolve(
        __dirname,
        "src/components/dashboard/salesmanagement/components/salestabs"
      ),
      "@salesmanagementActions": path.resolve(
        __dirname,
        "src/components/dashboard/salesmanagement/actions/index"
      ),
      "@salesmanagementReducers": path.resolve(
        __dirname,
        "src/components/dashboard/salesmanagement/reducers/index"
      ),

      /* PROFILE SETTINGS */
      "@profileSettings": path.resolve(
        __dirname,
        "src/components/dashboard/profilesettings/components/index"
      ),
      "@profileSettingsActions": path.resolve(
        __dirname,
        "src/components/dashboard/profilesettings/actions/index"
      ),
      "@profileSettingsReducers": path.resolve(
        __dirname,
        "src/components/dashboard/profilesettings/reducers/index"
      ),

      /* LANGUAGE */
      "@languageComponent": path.resolve(
        __dirname,
        "src/components/dashboard/language/components/index"
      ),
      "@languageActions": path.resolve(
        __dirname,
        "src/components/dashboard/language/actions/index"
      ),
      "@languageReducers": path.resolve(
        __dirname,
        "src/components/dashboard/language/reducers/index"
      ),

      /* CURRENCY */
      "@currency": path.resolve(
        __dirname,
        "src/components/dashboard/currency/components/index"
      ),
      "@currencyActions": path.resolve(
        __dirname,
        "src/components/dashboard/currency/actions/index"
      ),
      "@currencyReducers": path.resolve(
        __dirname,
        "src/components/dashboard/currency/reducers/index"
      ),

      /* PRODUCT MANAGEMNT */
      "@productmanagement": path.resolve(
        __dirname,
        "src/components/dashboard/productmanagement/components/index"
      ),
      "@productdetails": path.resolve(
        //Pruduct details page
        __dirname,
        "src/components/dashboard/productmanagement/components/productdetails/components/index"
      ),
      "@productmanagementActions": path.resolve(
        __dirname,
        "src/components/dashboard/productmanagement/actions/index"
      ),
      "@productmanagementReducers": path.resolve(
        __dirname,
        "src/components/dashboard/productmanagement/reducers/index"
      ),

      /* REVENUE REPORT */
      "@revenuereports": path.resolve(
        __dirname,
        "src/components/dashboard/revenuereports/components/index"
      ),
      "@revenuereportsrevenue": path.resolve(
        // revenuereports -> revenue
        __dirname,
        "src/components/dashboard/revenuereports/components/revenue/index"
      ),
      "@revenuereportssales": path.resolve(
        // revenuereports -> sales
        __dirname,
        "src/components/dashboard/revenuereports/components/sales/index"
      ),
      "@revenuereportstransaction": path.resolve(
        // revenuereports -> transaction
        __dirname,
        "src/components/dashboard/revenuereports/components/transaction/index"
      ),
      "@revenuereportsActions": path.resolve(
        __dirname,
        "src/components/dashboard/revenuereports/actions/index"
      ),
      "@revenuereportsReducers": path.resolve(
        __dirname,
        "src/components/dashboard/revenuereports/reducers/index"
      ),

      /* COUPON MANAGMENT */
      "@couponsmanagement": path.resolve(
        __dirname,
        "src/components/dashboard/couponsmanagement/components/index"
      ),
      "@couponsmanagementActions": path.resolve(
        __dirname,
        "src/components/dashboard/couponsmanagement/actions/index"
      ),
      "@couponsmanagementReducers": path.resolve(
        __dirname,
        "src/components/dashboard/couponsmanagement/reducers/index"
      ),

      /* DISCOUNT MANAGMENT */
      "@discountsmanagement": path.resolve(
        __dirname,
        "src/components/dashboard/discountsmanagement/components/index"
      ),
      "@discountsmanagementActions": path.resolve(
        __dirname,
        "src/components/dashboard/discountsmanagement/actions/index"
      ),
      "@discountsmanagementReducers": path.resolve(
        __dirname,
        "src/components/dashboard/discountsmanagement/reducers/index"
      ),

      /* REVIEW AND RETINGS */
      "@ratingsreviews": path.resolve(
        __dirname,
        "src/components/dashboard/ratingsreviews/components/index"
      ),
      "@ratingsreviewsActions": path.resolve(
        __dirname,
        "src/components/dashboard/ratingsreviews/actions/index"
      ),
      "@ratingsreviewsReducers": path.resolve(
        __dirname,
        "src/components/dashboard/ratingsreviews/reducers/index"
      ),

      /* FAQ SUPPORT */
      "@faqsupport": path.resolve(
        __dirname,
        "src/components/dashboard/faqsupport/components/index"
      ),
      "@faqsupportActions": path.resolve(
        __dirname,
        "src/components/dashboard/faqsupport/actions/index"
      ),
      "@faqsupportReducers": path.resolve(
        __dirname,
        "src/components/dashboard/faqsupport/reducers/index"
      ),

      /* LEGAL DOCS */
      "@legaldocs": path.resolve(
        __dirname,
        "src/components/dashboard/legaldocs/components/index"
      ),
      "@legaldocstermsncons": path.resolve(
        // Legaldocs -> termsncons
        __dirname,
        "src/components/dashboard/legaldocs/components/termsncons/index"
      ),
      "@legaldocsprivacypolicy": path.resolve(
        // Legaldocs -> privacypolicy
        __dirname,
        "src/components/dashboard/legaldocs/components/privacypolicy/index"
      ),
      "@legaldocssalespolicy": path.resolve(
        // Legaldocs -> salespolicy
        __dirname,
        "src/components/dashboard/legaldocs/components/salespolicy/index"
      ),
      "@legaldocsaboutus": path.resolve(
        // Legaldocs -> aboutus
        __dirname,
        "src/components/dashboard/legaldocs/components/aboutus/index"
      ),
      "@legaldocsActions": path.resolve(
        __dirname,
        "src/components/dashboard/legaldocs/actions/index"
      ),
      "@legaldocsReducers": path.resolve(
        __dirname,
        "src/components/dashboard/legaldocs/reducers/index"
      ),

      /* APP SUGGESTIONS */
      "@appsuggestions": path.resolve(
        __dirname,
        "src/components/dashboard/appsuggestions/components/index"
      ),
      "@appsuggestionsActions": path.resolve(
        __dirname,
        "src/components/dashboard/appsuggestions/actions/index"
      ),
      "@appsuggestionsReducers": path.resolve(
        __dirname,
        "src/components/dashboard/appsuggestions/reducers/index"
      ),
      // Payment Response
      "@paysuccess": path.resolve(
        __dirname,
        "src/components/paymentresponse/pay-success"
      ),
      "@payfail": path.resolve(
        __dirname,
        "src/components/paymentresponse/pay-fail"
      ),
      /* SIDE BAR */
      "@sidebar": path.resolve(
        __dirname,
        "src/components/dashboard/sidebar/components/index"
      ),
      "@sidebarActions": path.resolve(
        __dirname,
        "src/components/dashboard/sidebar/actions/index"
      ),
      "@sidebarReducers": path.resolve(
        __dirname,
        "src/components/dashboard/sidebar/reducers/index"
      ),
      // Main Dashboard
      "@maindashBoard": path.resolve(
        __dirname,
        "src/components/dashboard/dashboardmain/components/index"
      ),
      "@maindashBoardActions": path.resolve(
        __dirname,
        "src/components/dashboard/dashboardmain/actions/index"
      ),
      "@maindashBoardReducers": path.resolve(
        __dirname,
        "src/components/dashboard/dashboardmain/reducers/index"
      ),
      "@mainSearch": path.resolve(
        __dirname,
        "src/components/dashboard/search/components/index"
      ),
      // My contacts
      "@myContacts": path.resolve(
        __dirname,
        "src/components/dashboard/mycontacts/components/index"
      ),
      "@myContactsActions": path.resolve(
        __dirname,
        "src/components/dashboard/mycontacts/actions/index"
      ),
      "@myContactsReducers": path.resolve(
        __dirname,
        "src/components/dashboard/mycontacts/reducers/index"
      ),
      // Credit Addon
      "@creditAddon": path.resolve(
        __dirname,
        "src/components/dashboard/creditaddon/components/index"
      ),
      "@creditAddonActions": path.resolve(
        __dirname,
        "src/components/dashboard/creditaddon/actions/index"
      ),
      "@creditAddonReducers": path.resolve(
        __dirname,
        "src/components/dashboard/creditaddon/reducers/index"
      ),
      // Upgrade
      "@upgradePlan": path.resolve(
        __dirname,
        "src/components/dashboard/upgradeplan/components/index"
      ),
      "@upgradePlanActions": path.resolve(
        __dirname,
        "src/components/dashboard/upgradeplan/actions/index"
      ),
      "@upgradePlanReducers": path.resolve(
        __dirname,
        "src/components/dashboard/upgradeplan/reducers/index"
      ),
      "@upgradePricing": path.resolve(
        __dirname,
        "src/components/dashboard/upgradeplan/components/upgradepricing"
      ),
      // Privacy Policy
      "@privacypolicy": path.resolve(
        __dirname,
        "src/components/dashboard/privacypolicy/index"
      ),
      // Terms of use
      "@termsofuse": path.resolve(
        __dirname,
        "src/components/dashboard/termsofuse/index"
      ),
      // Chrome privacy
      "@chromeprivacy": path.resolve(
        __dirname,
        "src/components/dashboard/chromeprivacy/index"
      ),
      // ScheduleDemo
      "@scheduledemo": path.resolve(
        __dirname,
        "src/components/dashboard/scheduledemo/components/index"
      ),
      "@scheduledemoActions": path.resolve(
        __dirname,
        "src/components/dashboard/scheduledemo/actions/index"
      ),
      // Settings
      "@mainSettings": path.resolve(
        __dirname,
        "src/components/dashboard/settings/components/index"
      ),
      "@mainSettingsReducers": path.resolve(
        __dirname,
        "src/components/dashboard/settings/reducers/index"
      ),
      "@mainSettingsActions": path.resolve(
        __dirname,
        "src/components/dashboard/settings/actions/index"
      ),
      // setting myaccounts
      "@settingsMyaccount": path.resolve(
        __dirname,
        "src/components/dashboard/settings/components/myaccounts/components/index"
      ),
      "@settingsMyaccountActions": path.resolve(
        __dirname,
        "src/components/dashboard/settings/components/myaccounts/actions/index"
      ),
      "@settingsMyaccountReducers": path.resolve(
        __dirname,
        "src/components/dashboard/settings/components/myaccounts/reducers/index"
      ),
      // setting password
      "@settingsPassword": path.resolve(
        __dirname,
        "src/components/dashboard/settings/components/password/components/index"
      ),
      "@settingsPasswordAction": path.resolve(
        __dirname,
        "src/components/dashboard/settings/components/password/actions/index"
      ),
      // manage seats
      "@settingsManageseats": path.resolve(
        __dirname,
        "src/components/dashboard/settings/components/manageseats/components/index"
      ),
      "@manageseatAction": path.resolve(
        __dirname,
        "src/components/dashboard/settings/components/manageseats/actions/index"
      ),
      "@manageseatReducers": path.resolve(
        __dirname,
        "src/components/dashboard/settings/components/manageseats/reducers/index"
      ),
      // billing
      "@settingsBilling": path.resolve(
        __dirname,
        "src/components/dashboard/settings/components/billing/components/index"
      ),
      "@settingsBillingActions": path.resolve(
        __dirname,
        "src/components/dashboard/settings/components/billing/actions/index"
      ),
      "@settingsBillingReducers": path.resolve(
        __dirname,
        "src/components/dashboard/settings/components/billing/reducers/index"
      ),
      // plans
      "@settingsPlans": path.resolve(
        __dirname,
        "src/components/dashboard/settings/components/plans/components/index"
      ),
      "@settingsPlansActions": path.resolve(
        __dirname,
        "src/components/dashboard/settings/components/plans/actions/index"
      ),
      "@settingsPlansReducers": path.resolve(
        __dirname,
        "src/components/dashboard/settings/components/plans/reducers/index"
      ),
      // usage
      "@settingsUsage": path.resolve(
        __dirname,
        "src/components/dashboard/settings/components/usage/components/index"
      ),
      "@settingsUsageActions": path.resolve(
        __dirname,
        "src/components/dashboard/settings/components/usage/actions/index"
      ),
      "@settingsUsageReducers": path.resolve(
        __dirname,
        "src/components/dashboard/settings/components/usage/reducers/index"
      ),
      // api
      "@settingsApi": path.resolve(
        __dirname,
        "src/components/dashboard/settings/components/api/components/index"
      ),
      "@settingsApiActions": path.resolve(
        __dirname,
        "src/components/dashboard/settings/components/api/actions/index"
      ),
      "@settingsApiReducers": path.resolve(
        __dirname,
        "src/components/dashboard/settings/components/api/reducers/index"
      ),
      // privacy
      "@settingsPrivacy": path.resolve(
        __dirname,
        "src/components/dashboard/settings/components/privacy/components/index"
      ),
    },
    extensions: ["", ".json", ".js", ".jsx", ".ejs", ".css"],
  },
};
