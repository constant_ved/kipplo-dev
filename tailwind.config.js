module.exports = {
  purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      textColor: {
        primary: "#4470FB",
        primaryLight: "#0C88EF",
        primaryHover: "#EEF2FF",
        secondary: "#303E4B",
        ternary: "#969FA7",
        quaternary: "#f9fafb",
        custWhite: "#FFFFFF",
        custblack: "#000000",
        danger: "#b91c1c",
        success: "#54d695",
        warning: "#facc15",
        newtheme: "#303e4b",
        e6e9f0: "#E6E9F0",
        semiGray: "#344054",
        gray500: "#667085",
        gray700: "#344054",
        mainGray: "#101828",
        gray6B7280: "#6B7280",
        green027A48: "027A48",
      },
      maxHeight: {
        "screen-80": "80vh",
      },
      ringColor: {
        primary: "#4470FB",
        primaryLight: "#dbeafe",
        secondary: "#303E4B",
        ternary: "#969FA7",
        quaternary: "#f9fafb",
        custblack: "#000000",
        custWhite: "#FFFFFF",
        warning: "#facc15",
      },
      divideColor: {
        primary: "#4470FB",
        primaryLight: "#0C88EF",
        primaryHover: "#EEF2FF",
        secondary: "#303E4B",
        ternary: "#969FA7",
        quaternary: "#f9fafb",
        custWhite: "#FFFFFF",
        custblack: "#000000",
        danger: "#b91c1c",
        success: "#54d695",
        warning: "#facc15",
        newtheme: "#303e4b",
      },
      borderColor: {
        primary: "#4470FB",
        primaryLight: "#0C88EF",
        primaryHover: "#EEF2FF",
        secondary: "#303E4B",
        ternary: "#969FA7",
        quaternary: "#f9fafb",
        warning: "#facc15",
        e8ebf1: "e8ebf1",
        whiteboxgray: "D0D5DD",
      },
      placeholderColor: {
        primary: "#4470FB",
        secondary: "#303E4B",
        ternary: "#969FA7",
        quaternary: "#f9fafb",
        warning: "#facc15",
      },
      backgroundColor: (theme) => ({
        primary: "#4470FB",
        primaryLight: "#dbeafe",
        primaryHover: "#EEF2FF",
        secondary: "#F0F5FA",
        ternary: "#969FA7",
        ternaryHover: "#ecedef",
        ternaryHoverDiv: "#7d7f82",
        quaternary: "#f9fafb",
        custWhite: "#FFFFFF",
        custblack: "#000000",
        custblackHover: "#40424c",
        danger: "#FF5858",
        dangerHover: "#ea7d7d",
        success: "#54d695",
        chatbg: "#F9FAFB",
        warning: "#facc15",
        e6e9f0: "#E6E9F0",
        homegray: "#FAFBFC",
      }),
      height: {
        h70: "70%",
        h69: "69%",
      },
    },
    screens: {
      xs: "320px",
      sm: "640px",
      // => @media (min-width: 640px) { ... }

      md: "768px",
      // => @media (min-width: 768px) { ... }

      lg: "1024px",
      // => @media (min-width: 1024px) { ... }

      xl: "1280px",
      // => @media (min-width: 1280px) { ... }

      "2xl": "1536px",
      // => @media (min-width: 1536px) { ... }
    },
    fontFamily: {
      body: ["Inter"],
    },
    animation: {
      "spin-slow": "spin 3s linear infinite",
      "spin-medium": "spin 2s linear infinite",
      ping: "ping 1s cubic-bezier(0, 0, 0.2, 1) infinite",
    },
  },
  variants: {
    extend: {},
  },
  plugins: [require("@tailwindcss/forms"), require("tailwind-scrollbar-hide")],
};
