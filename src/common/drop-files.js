import React, { Component } from "react";

import Dropzone from "react-dropzone";
import LocaleStrings from "@language";

export default class AllFilesDrop extends Component {
  constructor(props) {
    super(props);

    this.imageCallback = null;

    this.state = {
      imagePreviewUrl: props.imagePreviewUrl,
      filepath: props.filepath,
    };
    this.resetImage = this.resetImage.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps) {
      if (nextProps.filepath !== "" && this.state.filepath === "") {
        this.setState({ filepath: nextProps.filepath });
      }
    }
  }

  onDropFile = (files) => {
    var that = this;
    let file = files[0];

    let reader = new FileReader();
    reader.onloadend = () => {
      var item = reader.result;
      var data = { file: item, filename: file.name.replace(/ /g, "-") };
      that.props.onFileSave(data);
      that.props.onFileChnageLocally(true);
    };
    reader.readAsDataURL(file);
  };

  resetImage = () => {
    this.setState({ filepath: "" });
    this.props.onFileSave("");
    this.props.onFileChnageLocally(true);
  };

  render() {
    let fileUrl = this.state.filepath;
    let { fileName, fileOld, accept } = this.props;

    var hideDropZoneClass = "";
    var hideControlButton = "hide-feature";

    if (fileUrl && fileUrl !== "file object not found") {
      hideDropZoneClass = "hide-feature";
      hideControlButton = "";
    }

    var aStyle = {
      width: this.props.width,
      height: this.props.height,
    };

    var dropStyle = {
      width: this.props.width,
      height: this.props.height,
    };

    return (
      <div className="col-md-6 mb-10">
        <Dropzone
          onDrop={this.onDropFile}
          accept={accept ? accept : ""}
          multiple={false}
          className={`${this.props.className} ${hideDropZoneClass}`}
          style={dropStyle}>
          {this.props.innerText}
        </Dropzone>
        {fileOld ? (
          <a
            className={`${hideControlButton} theme-color-text`}
            href={`${fileUrl}`}
            target="_blank"
            title="Download">
            <div className="file-name-display" style={aStyle}>
              {fileName}
            </div>
          </a>
        ) : (
          <div
            className={`${hideControlButton} file-name-display theme-color-text`}
            style={aStyle}>
            {fileName}
          </div>
        )}

        <div className={hideControlButton}>
          <button
            bsStyle=""
            type="button"
            className="btn btn-custom-blue mt-10"
            onClick={this.resetImage}>
            {" "}
            {LocaleStrings.button_upload_new_file}{" "}
          </button>
        </div>
      </div>
    );
  }
}
