import React from "react";
import LocaleStrings from "@language";
import toast from "react-hot-toast";
const FetureUnderConstructionToast = (props) => {
  return (
    <div
      className={`${
        toast.visible ? "animate-enter" : "animate-leave"
      } max-w-md w-full bg-white shadow-lg rounded-lg pointer-events-auto flex ring-1 ring-black ring-opacity-5`}>
      <div className="flex-1 w-0 p-4">
        <div className="flex items-start">
          <div className="flex-shrink-0 pt-0.5"></div>
          <div className="ml-3 flex-1">
            <p className="text-sm font-medium text-primary">{props.header}</p>
            <p className="mt-1 text-sm text-secondary">{props.subheader}</p>
          </div>
        </div>
      </div>
      <div className="flex border-l border-gray-200">
        <button
          onClick={() => toast.dismiss(toast.id)}
          className="w-full border border-transparent rounded-none rounded-r-lg p-4 flex items-center justify-center text-sm font-medium text-primary hover:text-primaryLight">
          {LocaleStrings.ok}
        </button>
      </div>
    </div>
  );
};

export default FetureUnderConstructionToast;
