import React, { Component } from "react";
import { connect } from "react-redux";
import { logout } from "Actions";

class Reset extends Component {
  constructor(props) {
    super(props);
    this.props.logout(this.props.session);
  }

  render() {
    return <div />;
  }
}

function mapStateToProps(state) {
  return { session: state.session };
}

export default connect(mapStateToProps, { logout })(Reset);
