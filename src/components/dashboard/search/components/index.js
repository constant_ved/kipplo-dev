import React, { Component } from "react";
import { connect } from "react-redux";
import LocaleStrings from "@language";

class MainSearch extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      loaderDesc: LocaleStrings.preparing_currency,
    };

    // All Binded Functions
  }

  render() {
    return <>This section is under development</>;
  }
}

var mapStateToProps = (state) => ({
  isLoggedIn: state.isLoggedIn,
  user: state.user,
  session: state.session,
});

export default connect(mapStateToProps, {})(MainSearch);
