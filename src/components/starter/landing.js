import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

class Landing extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {}

  UNSAFE_componentWillMount() {
    if (this.props.isLoggedIn === true) {
      this.props.history.push("/dashboard");
    } else {
      this.props.history.push("/login");
    }
  }

  render() {
    return (
      <div className="text-center landing-container">
        <Link to="/login" className="btn btn-primary">
          Login
        </Link>
        <br />
        <Link to="/signup" className="btn btn-primary">
          Sign Up
        </Link>
      </div>
    );
  }
}

function mapStateToProps({ isLoggedIn }) {
  return { isLoggedIn };
}

export default connect(mapStateToProps)(Landing);
