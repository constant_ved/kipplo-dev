import React, { Component } from "react";

class LoginErrorBar extends Component {
  render() {
    return (
      <div className="alert animated fadeIn" style={this.props.style}>
        {this.props.alertMessage}
      </div>
    );
  }
}

export default LoginErrorBar;
